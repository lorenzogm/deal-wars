/* eslint-disable consistent-return */

import SimpleSchema from 'simpl-schema';

// meteor
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

import Status from './enums/Status';

const Deals = new Mongo.Collection('Deals');

if (Meteor.isServer) {
  Deals._ensureIndex({ 'product._id': 1, shopId: 1 }, { unique: true });
}

Deals.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Deals.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Deals.schema = new SimpleSchema({
  createdAt: {
    type: String,
    autoValue() {
      if (this.isInsert) return new Date().toISOString();
    },
  },
  productId: String,
  shopId: String,
  price: Number,
  coupon: String,
  status: Status.enumValues.map(element => element.name),
  queuedAt: String,
});

Deals.attachSchema(Deals.schema);

export default Deals;
