// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// app
import rateLimit from '../../../app/modules/rate-limit';

// deals
import Deals from './Deals';

Meteor.methods({
  'deals.insert': function dealsInsert(doc) {
    check(doc, {
      title: String,
      body: String,
    });

    try {
      return Deals.insert({ owner: this.userId, ...doc });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'deals.update': function dealsUpdate(doc) {
    check(doc, {
      _id: String,
      title: String,
      body: String,
    });

    try {
      const dealId = doc._id;
      Deals.update(dealId, { $set: doc });
      return dealId; // Return _id so we can redirect to deal after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'deals.remove': function dealsRemove(dealId) {
    check(dealId, String);

    try {
      return Deals.remove(dealId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: ['deals.insert', 'deals.update', 'deals.remove'],
  limit: 5,
  timeRange: 1000,
});
