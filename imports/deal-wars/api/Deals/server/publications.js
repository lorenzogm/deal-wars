// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// spiders
import Deals from '../Deals';

Meteor.publish('deals', () => {
  const deals = Deals.find();
  return deals;
});

Meteor.publish('deals.active', () => {
  const deals = Deals.find({ status: 'ACTIVE' }, { sort: { updatedAt: 1, createdAt: 1 } });
  return deals;
});

// Note: spiders.view is also used when editing an existing document.
// Meteor.publish('spiders.view', function spidersView(spiderId) {
//   check(spiderId, String);
//   return Deals.find({ _id: spiderId, owner: this.userId });
// });
