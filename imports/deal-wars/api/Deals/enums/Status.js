import { Enum } from 'enumify';

class Status extends Enum {}

Status.initEnum(['ACTIVED', 'FINISHED']);

export default Status;
