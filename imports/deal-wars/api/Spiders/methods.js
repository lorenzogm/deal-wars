// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// app
import rateLimit from '../../../app/modules/rate-limit';

// spiders
import Spiders from './Spiders';

Meteor.methods({
  'spiders.insert': function spidersInsert(doc) {
    check(doc, {
      title: String,
      body: String,
    });

    try {
      return Spiders.insert({ owner: this.userId, ...doc });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'spiders.update': function spidersUpdate(doc) {
    check(doc, {
      _id: String,
      title: String,
      body: String,
    });

    try {
      const spiderId = doc._id;
      Spiders.update(spiderId, { $set: doc });
      return spiderId; // Return _id so we can redirect to spider after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'spiders.remove': function spidersRemove(spiderId) {
    check(spiderId, String);

    try {
      return Spiders.remove(spiderId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'spiders.insert',
    'spiders.update',
    'spiders.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
