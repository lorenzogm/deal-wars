import { Enum } from 'enumify';

class Types extends Enum {}

Types.initEnum(['LIST', 'PRODUCT']);

export default Types;
