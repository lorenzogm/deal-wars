// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// spiders
import Spiders from '../Spiders';

Meteor.publish('spiders', function spiders() {
  return Spiders.find({ owner: this.userId });
});

// Note: spiders.view is also used when editing an existing document.
Meteor.publish('spiders.view', function spidersView(spiderId) {
  check(spiderId, String);
  return Spiders.find({ _id: spiderId, owner: this.userId });
});
