/* eslint-disable consistent-return */

import SimpleSchema from 'simpl-schema';

// meteor
import { Mongo } from 'meteor/mongo';

import Markets from '../enums/Markets';
import Shops from '../enums/Shops';
import Types from './enums/Types';

const Spiders = new Mongo.Collection('Spiders');

Spiders.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Spiders.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Spiders.schema = new SimpleSchema({
  market: {
    type: Array,
    optional: true,
  },
  'market.$': {
    type: String,
    allowedValues: Markets.enumValues.map(element => element.name),
  },
  name: String,
  shop: {
    type: String,
    allowedValues: Shops.enumValues.map(element => element.name),
  },
  type: {
    type: String,
    allowedValues: Types.enumValues.map(element => element.name),
  },
  urls: {
    type: Array,
    optional: true,
  },
  'urls.$': String,
  allowedDomains: {
    type: Array,
    optional: true,
  },
  'allowedDomains.$': String,
  queuedAt: {
    type: Date,
    optional: true,
  },
  frequency: SimpleSchema.Integer,
  // createdAt: {
  //   type: String,
  //   label: 'The date this spider was created.',
  //   autoValue() {
  //     /*  */
  //     if (this.isInsert) return new Date().toISOString();
  //   },
  // },
  // updatedAt: {
  //   type: String,
  //   label: 'The date this spider was last updated.',
  //   autoValue() {
  //     if (this.isInsert || this.isUpdate) return new Date().toISOString();
  //   },
  // },
});

Spiders.attachSchema(Spiders.schema);

export default Spiders;
