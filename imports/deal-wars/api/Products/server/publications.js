// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// spiders
import Products from '../Products';

Meteor.publish('products', () => {
  const products = Products.find();
  return products;
});

// Note: spiders.view is also used when editing an existing document.
// Meteor.publish('spiders.view', function spidersView(spiderId) {
//   check(spiderId, String);
//   return Products.find({ _id: spiderId, owner: this.userId });
// });
