/* eslint-disable consistent-return */

import SimpleSchema from 'simpl-schema';

// meteor
import { Mongo } from 'meteor/mongo';

import Markets from '../enums/Markets';
import Shops from '../enums/Shops';
// import Status from './enums/Types';

const Products = new Mongo.Collection('Products');

Products.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Products.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Products.schema = new SimpleSchema({
  createdAt: {
    type: String,
    autoValue() {
      if (this.isInsert) return new Date().toISOString();
    },
  },
  updatedAt: {
    type: String,
    autoValue() {
      if (this.isInsert || this.isUpdate) return new Date().toISOString();
    },
  },
  market: {
    type: Array,
    optional: true,
  },
  'market.$': {
    type: String,
    allowedValues: Markets.enumValues.map(element => element.name),
  },
  shop: {
    type: String,
    allowedValues: Shops.enumValues.map(element => element.name),
  },
  name: String,
  sku: String,
  // url: String,
  img: String,
  // category: Object,
  // 'category.name': String,
  // 'category.url': String,
  // status: {
  //   type: String,
  //   allowedValues: Status.enumValues.map(element => element.name),
  // },
});

Products.attachSchema(Products.schema);

export default Products;
