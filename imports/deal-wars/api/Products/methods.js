// meteor
import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

// app
import rateLimit from '../../../app/modules/rate-limit';

// products
import Products from './Products';

Meteor.methods({
  'products.insert': function productsInsert(doc) {
    check(doc, {
      title: String,
      body: String,
    });

    try {
      return Products.insert({ owner: this.userId, ...doc });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'products.update': function productsUpdate(doc) {
    check(doc, {
      _id: String,
      title: String,
      body: String,
    });

    try {
      const productId = doc._id;
      Products.update(productId, { $set: doc });
      return productId; // Return _id so we can redirect to product after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'products.remove': function productsRemove(productId) {
    check(productId, String);

    try {
      return Products.remove(productId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: ['products.insert', 'products.update', 'products.remove'],
  limit: 5,
  timeRange: 1000,
});
