import { Enum } from 'enumify';

class Markets extends Enum {}

Markets.initEnum(['ES', 'EN']);

export default Markets;
