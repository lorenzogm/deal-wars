import { Enum } from 'enumify';

class Shops extends Enum {}

Shops.initEnum(['AMAZON', 'GEARBEST']);

export default Shops;
