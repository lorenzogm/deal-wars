// meteor
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import { Bert } from 'meteor/themeteorchef:bert';

// react
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Table, Alert, Button } from 'react-bootstrap';
import { timeago, monthDayYearAtTime } from '@cleverbeagle/dates';

// app
import Loading from '../../../../app/ui/components/Loading/Loading';

// documents
import DealsCollection from '../../../api/Deals/Deals';
import './Home.scss';

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.handleRemove = this.handleRemove.bind(this);
  }
  handleRemove(documentId) {
    if (confirm('Are you sure? This is permanent!')) {
      Meteor.call('documents.remove', documentId, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          Bert.alert('Document deleted!', 'success');
        }
      });
    }
  }

  render() {
    const { loading, match, deals } = this.props;

    if (loading) {
      return <Loading />;
    }

    return (
      <div className="Home">
        <div className="page-header clearfix">
          <h4 className="pull-left">Deals</h4>
          <Link className="btn btn-success pull-right" to={`${match.url}/new`}>
            Add Document
          </Link>
        </div>
        {deals.length ? (
          <Table responsive>
            <thead>
              <tr>
                <th>IMG</th>
                <th>Market</th>
                <th>Shop</th>
                <th>Name</th>
                <th>Sku</th>
                <th />
                <th />
              </tr>
            </thead>
            <tbody>
              {deals.map(({ _id, product, shopId }) => (
                <tr key={_id}>
                  <td>
                    <img src={product.img} alt={product.name} />
                  </td>
                  <td>{product.market}</td>
                  <td>{shopId}</td>
                  <td>{product.name}</td>
                  <td>{product.prices[shopId].sku}</td>
                  <td>
                    <Button
                      bsStyle="primary"
                      onClick={() => history.push(`${match.url}/${_id}`)}
                      block
                    >
                      View
                    </Button>
                  </td>
                  <td>
                    <Button bsStyle="danger" onClick={() => this.handleRemove(_id)} block>
                      Delete
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          <Alert bsStyle="warning">No documents yet!</Alert>
        )}
      </div>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool.isRequired,
  deals: PropTypes.arrayOf(PropTypes.object).isRequired,
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker(() => {
  const subscription = Meteor.subscribe('deals.active');

  return {
    loading: !subscription.ready(),
    deals: DealsCollection.find().fetch(),
  };
})(Home);
