// api
import '../../api/Deals/methods';
import '../../api/Deals/server/publications';
import '../../api/Products/methods';
import '../../api/Products/server/publications';
import '../../api/Spiders/methods';
import '../../api/Spiders/server/publications';

// startup
import './migrations';
