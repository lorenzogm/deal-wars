// meteor
import { Migrations } from 'meteor/percolate:migrations';

import Spiders from '../../../api/Spiders/Spiders';

Migrations.add({
  version: 1,
  name: 'Add GearBest spiders',
  up: () => {
    const spiders = [
      {
        market: 'ES',
        name: 'GEARBEST_FLASH_SALE',
        shop: 'GEARBEST',
        type: 'LIST',
        urls: ['https://es.gearbest.com/flash-sale.html'],
        allowedDomains: ['es.gearbest.com'],
        queuedAt: new Date(),
        frequency: 60, // In minutes
      },
      {
        market: 'EN',
        name: 'GEARBEST_FLASH_SALE',
        shop: 'GEARBEST',
        type: 'LIST',
        urls: ['https://gearbest.com/flash-sale.html'],
        allowedDomains: ['gearbest.com'],
        queuedAt: new Date(),
        frequency: 60, // In minutes
      },
      {
        name: 'GEARBEST_PRODUCT',
        shop: 'GEARBEST',
        type: 'PRODUCT',
      },
    ];

    Spiders.batchInsert(spiders);
  },
  down: () => {
    Spiders.remove({});
  },
});
// Videos._ensureIndex({ owner_id: 1, likesCount: 1 });
