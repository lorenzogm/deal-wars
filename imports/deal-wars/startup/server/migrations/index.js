import { Meteor } from 'meteor/meteor';
import { Migrations } from 'meteor/percolate:migrations';

import './001-add-gearbest-spiders';

Meteor.startup(() => {
  Migrations.migrateTo('latest');
});
