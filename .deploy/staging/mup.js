module.exports = {
  servers: {
    staging: {
      host: '139.59.165.10',
      username: 'lorenzogm',
      // pem: '~/.ssh/id_rsa',
    },
  },

  meteor: {
    name: 'deal-wars',
    path: '../../',

    docker: {
      // change to 'kadirahq/meteord' if your app is using Meteor 1.3 or older
      image: 'abernix/meteord:node-8.4.0-base',
      imagePort: 3366,
    },
    deployCheckWaitTime: 120,

    servers: {
      staging: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      PORT: '3366',
      NODE_ENV: 'production',
      ROOT_ENV: 'staging',
      ROOT_URL: 'http://dealwars.elegantdev.com',
      MONGO_URL: 'mongodb://localhost/meteor',
      // MONGO_URL: 'mongodb://10.0.199.61:27017/deal-wars-staging',
      // MAIL_URL: 'smtp://192.168.1.126:25',
      // NODE_TLS_REJECT_UNAUTHORIZED: '0',
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: false,
  },

  mongo: {
    version: '3.4.1',
    servers: {
      staging: {},
    },
  },

  proxy: {
    domains: 'dealwars.elegantdev.com',
    ssl: {
      letsEncryptEmail: 'dev@lorenzogm.com',
      forceSSL: true,
    },
    shared: {
      // The port number to listen to for http connections. Default 80.
      httpPort: 3366,
      // The port to listen for https connections. Default is 443.
      httpsPort: 443,
      // Set proxy wide upload limit. Setting 0 will disable the limit.
      clientUploadLimit: '10M',
      // Environment variables for nginx proxy
      env: {
        DEFAULT_HOST: 'dealwars.eleagntdev.com',
      },
      // env for the jrcs/letsencrypt-nginx-proxy-companion container
      envLetsEncrypt: {
        // Directory URI for the CA ACME API endpoint
        // (default: https://acme-v01.api.letsencrypt.org/directory).
        // If you set it's value to
        // https://acme-staging.api.letsencrypt.org/directory
        // letsencrypt will use test servers that
        // don't have the 5 certs/week/domain limits.
        ACME_CA_URI: 'https://acme-v01.api.letsencrypt.org/directory',

        // Set it to true to enable debugging of the entrypoint script and
        // generation of LetsEncrypt certificates,
        // which could help you pin point any configuration issues.
        DEBUG: true,
      },
    },
  },
};
