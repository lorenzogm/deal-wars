module.exports = {
  servers: {
    production: {
      host: '139.59.165.10',
      username: 'lorenzogm',
    },
  },

  app: {
    name: 'deal-wars',
    path: '../../',

    servers: {
      production: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      NODE_ENV: 'production',
      ROOT_ENV: 'production',
      ROOT_URL: 'http://dealwars.elegantdev.com',
      MONGO_URL: 'mongodb://10.0.199.63:27017/benchpro-production',
      MAIL_URL: 'smtp://192.168.1.126:25',
      NODE_TLS_REJECT_UNAUTHORIZED: '0',
    },

    docker: {
      // change to 'kadirahq/meteord' if your app is using Meteor 1.3 or older
      image: 'abernix/meteord:base',
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: false,
  },

  proxy: {
    domains: 'benchpro.simon-kucher.com,www.benchpro.simon-kucher.com',
    ssl: {
      crt: './bundle.crt',
      key: './private.key',
      forceSSL: true,
    },
  },
};
